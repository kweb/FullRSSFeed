#!/usr/bin/env python
# -*- coding:UTF-8 -*- 

import feedparser

import urllib.request as ur_req

import re

from multiprocessing.pool import ThreadPool

from urllib.parse import urlparse,parse_qs

from bs4 import BeautifulSoup

from feedgen.feed import FeedGenerator

from http.server import BaseHTTPRequestHandler, HTTPServer

pool = ThreadPool(processes=2)

# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):

	def do_GET(self):	
		qs={}
		path = self.path
		if '?' in path:
			query = urlparse(self.path).query
			qs = parse_qs(query) 
		# Send response status code
		self.send_response(200)
		# Send headers
		self.send_header('Content-type','text/xml')
		self.end_headers()
		
		print(qs['feed_url'][0])
		print(qs['count'][0])

		#full_rss = rss(qs['feed_url'][0],qs['count'][0])

		

		

		async_result = pool.apply_async(rss,(qs['feed_url'][0],qs['count'][0])) # tuple of args for foo


		full_rss = async_result.get()  # get the return value from your function.
		# Send message back to client
		message = full_rss
		# Write content as utf-8 data
		#self.wfile.write(bytes(message, "utf8"))
		self.wfile.write(message)		
		return

def run():
  print('Starting server...')

  # Server settings
  # Choose port 8080, for port 80, which is normally used for a http server, you need root access
  server_address = ('127.0.0.1', 8081)
  httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
  print('Running server...')
  httpd.serve_forever()

def rss(feed_url,count):
	print(feed_url)
	print(count)
	d=feedparser.parse(feed_url)

	feed = d.feed

	fg = FeedGenerator()

	if 'title' in feed:
		fg.title(feed.title)
	if 'author' in feed:
		fg.author( {'name':feed.author,'email':feed.author_detail} )
	if 'logo' in feed:
		fg.logo(feed.icon)
	if 'subtitle' in feed:
		fg.subtitle(feed.subtitle)

	fg.link( href=feed_url, rel='self' )

	#for atom feed
	if 'id' in feed:
		fg.id(feed.id)
	if 'description' not in feed:
		fg.description('No description given')

	fg.language('en')

	ct = 0

	# Loop over all the entries
	for e in d.entries:
		#print(e)
	

		if ct == int(count):
			break

		url = e.links[0].href

		opener = ur_req.build_opener(ur_req.HTTPSHandler(debuglevel=0))
		
		#Some site requires user agent otherwise got 403 forbidden
		req = ur_req.Request(url,data=None,headers={'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:57.0) Gecko/20100101 Firefox/57.0'})

		response = opener.open(req,timeout=5)
		the_page = response.read()
		http_headers = response.info()

	
		soup = BeautifulSoup(the_page,"lxml")
		
		
		from_summary = False
		content_div = None
		
		
		#TODO check main tag as div#main or article tag
		tag_found = False
		tag_queries = [
		    ("div", {"class":"section"}),
		    ("article",{}),
		    ("div", {"id":"main"}),
		    ("div", {"class":"main"})
		]
		
		for i in range(len(tag_queries)):
			tag_query = tag_queries[i]
			soup_result = soup.find_all(tag_query[0], tag_query[1])
			if len(soup_result) > 0:
				soup = soup_result[0]
				tag_found = True
		#if there is a summary try to saerch content based on it.
		if tag_found == False:	
			if 'summary' in e:
			
				from_summary = True

				#Check if there is html tag in the summary
				TagRegex = re.compile(r'(<[a-z]*>)')
				result =TagRegex.findall(e.summary)

				summary_lenght = 0			

				if len(result) > 0:
					#remove at least A tag			
					summary_soup = BeautifulSoup(e.summary,"lxml")
			
					summary_content = summary_soup.find('p').get_text()

					summary_lenght = len(summary_content)

					#Blindly decided to search over the page 3 words of the summary 

					summary_part = re.split(r'\W+',summary_content,3)

					if len(summary_part) == 3 :
						search_txt = summary_part[0]+' '+summary_part[1] + ' ' + summary_part[2]

						content_div = soup.find(text=re.compile(search_txt))
					else:
						content_div = soup.find(text=re.compile(summary_content))

				else:
					summary_lenght = len(e.summary)
				
					summary_part = re.split(r'\W+',e.summary,3)

					search_txt = summary_part[0]+' '+summary_part[1] + ' ' + summary_part[2]

					content_div = soup.find(text=re.compile(search_txt))
			
					print(content_div)


		if content_div != None and from_summary:
			print('Based on summary')

			html_content = str(content_div.parent.parent)

			hc_obj = content_div.parent.parent

			print(len(html_content))
			print(summary_lenght)
		
			#if content lenght is superior to 3 times the summary
			#Could cause problem if article is not very long
			while len(hc_obj.get_text()) <= (summary_lenght*3):
				hc_obj = hc_obj.parent

			html_content = str(hc_obj)
		else:
			print('Based on html content')	
			# Remove unnecessary tag
			for tag in soup(["head","script", "style","header","nav","footer","button","input","select","textarea","aside","form","svg","iframe","[class=hidden]"]):
	    			tag.extract()		
			html_content = str(soup)

		
		fe = fg.add_entry()
		fe.id(url)
	
		fe.title(e.title)
		
		#atom feed doesn't have published date
		if 'date' in e:
			fe.pubdate(e.date)

		if 'published' in e:
			fe.pubdate(e.published)
		#fe.summary(e.summary)
		s = set(e)
		#
		#if 'summary_detail' in e:	
		fe.link(href=url)
		fe.content(html_content)
		
			
		ct = ct + 1

	return fg.rss_str(pretty=True)
	#fg.rss_file('fullrss.xml')


run()


